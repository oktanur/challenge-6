const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const path = require("path");

userRouter.get("/rock_paper_scs", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/rock_paper_scs.html"));
});

userRouter.get("/", userController.allUser); //get all users data
userRouter.get("/:idUser", userController.getUserBio); //get user biodata based on user's id
userRouter.get("/register", userController.renderRegister); //render register page
userRouter.get("/login", userController.renderLogin); //render login page
userRouter.post("/register", userController.registerUser); //register user
userRouter.post("/login", userController.loginUser); //login user
userRouter.put("/detail/:idUser", userController.updateOrCreateBio); //update or create user bio based on id
userRouter.post("/game", userController.userGames); //create users game histories
userRouter.get("/game/:idUser", userController.getUserHistory); //display user game history based on id

module.exports = userRouter;
