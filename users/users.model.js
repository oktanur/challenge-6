const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");
class UserModel {
  // get  user data
  getUsers = async () => {
    const userList = await db.User.findAll({ include: [db.UserBio] });
    return userList;
  };

  //   check whether user already exist
  isRegistered = async (username, email) => {
    const existUser = await db.User.findOne({
      where: {
        [Op.or]: [
          {
            username: username,
          },
          {
            email: email,
          },
        ],
      },
    });
    if (existUser) {
      return true;
    } else {
      return false;
    }
  };

  //   record new user
  recordUser = async (username, email, password) => {
    await db.User.create({
      username: username,
      email: email,
      password: md5(password),
    });
  };
  // Login authentication
  isAuthenticated = async (email, password) => {
    const sortedUsers = await db.User.findOne({
      where: {
        email: email,
        password: md5(password),
      },
    });
    console.log(sortedUsers);
    return sortedUsers;
  };

  // get user bio by id
  getUserById = async (idUser) => {
    const sortedUsers = await db.UserBio.findOne({
      where: {
        user_id: idUser,
      },
    });
    return sortedUsers;
  };

  // get user by id on Users table
  findUserId = async (idUser) => {
    const sortedUsers = await db.User.findOne({
      where: {
        id: idUser,
      },
    });
    if (sortedUsers) {
      return true;
    } else {
      return false;
    }
  };

  // create new user Bio
  createUserBio = async (fullname, gender, address, phoneNumber, idUser) => {
    await db.UserBio.create({
      fullname: fullname,
      gender: gender,
      address: address,
      phoneNumber: phoneNumber,
      user_id: idUser,
    });
  };

  // update user Bio
  updateUserBio = async (fullname, gender, address, phoneNumber, idUser) => {
    await db.UserBio.update(
      {
        fullname: fullname,
        gender: gender,
        address: address,
        phoneNumber: phoneNumber,
      },
      {
        where: {
          user_id: idUser,
        },
      }
    );
  };

  // create user game history
  userGameHis = async (user_id, result) => {
    await db.GameHistory.create({
      user_id: user_id,
      result: result,
    });
  };

  // get user id in GameHistory table
  getIdGameHistory = async (idUser) => {
    const userHistoryData = await db.GameHistory.findAll({
      where: {
        user_id: idUser,
      },
      include: [db.User],
    });
    return userHistoryData;
  };
}

module.exports = new UserModel();
