const userModel = require("./users.model");

class UserController {
  // render register page
  renderRegister = (req, res) => {
    res.render("register");
  };

  //   render login page
  renderLogin = (req, res) => {
    res.render("login");
  };
  // get all users
  allUser = async (req, res) => {
    const allUsers = await userModel.getUsers();
    return res.json(allUsers);
  };

  // register new user
  registerUser = async (req, res) => {
    let { username, email, password } = req.body;
    if (!username && !email && !password) {
      res.status(400);
      return res.json({ message: "Invalid username, email, and password" });
    } else if (!username && !email) {
      res.status(400);
      return res.json({ message: "Invalid username and email" });
    } else if (!username && !password) {
      res.status(400);
      return res.json({ message: "Invalid password username and password" });
    } else if (!email && !password) {
      res.status(400);
      return res.json({ message: "Invalid email and password" });
    } else if (!username) {
      res.status(400);
      return res.json({ message: "Invalid username" });
    } else if (!password) {
      res.status(400);
      return res.json({ message: "Invalid password" });
    } else if (!email) {
      res.status(400);
      return res.json({ message: "Invalid email" });
    }
    // find user already exist
    const existUser = await userModel.isRegistered(username, email);
    if (existUser) {
      res.status(400);
      return res.json({ message: "Username or Email is already exists" });
    }
    res.status(200);
    await userModel.recordUser(username, email, password);
    return res.json({ message: "Successfully adding new user" });
  };

  //   Login Authentication
  loginUser = async (req, res) => {
    let { email, password } = req.body;
    const sortedUsers = await userModel.isAuthenticated(email, password);
    if (sortedUsers) {
      res.status(200);
      return res.json(sortedUsers);
    } else {
      res.status(400);
      return res.json({ message: "Invalid credentials" });
    }
  };

  // get single user bio
  getUserBio = async (req, res) => {
    const { idUser } = req.params;
    try {
      const user = await userModel.getUserById(idUser);
      if (user) {
        res.status(200);
        return res.json(user);
      } else {
        res.status(400);
        return res.json({
          message: `biodata with user id ${idUser} is not found`,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400);
      return res.json({
        message: `biodata with id ${idUser} is not found`,
      });
    }
  };

  // update or create user bio
  updateOrCreateBio = async (req, res) => {
    const { idUser } = req.params;
    const { fullname, gender, address, phoneNumber } = req.body;
    const userId = await userModel.findUserId(idUser);
    const userBioId = await userModel.getUserById(idUser);
    try {
      if (userId && !userBioId) {
        if (fullname === undefined || fullname === "") {
          res.status(400);
          return res.json({ message: "Invalid fullname" });
        } else if (gender === undefined || gender === "") {
          res.status(400);
          return res.json({ message: "Invalid gender" });
        } else if (address === undefined || address === "") {
          res.status(400);
          return res.json({ message: "Invalid address" });
        } else if (phoneNumber === undefined || phoneNumber === "") {
          res.status(400);
          return res.json({ message: "Invalid phone number" });
        }
        await userModel.createUserBio(
          fullname,
          gender,
          address,
          phoneNumber,
          idUser
        );
        res.status(200);
        return res.json({ message: "New user bio is added" });
      } else if (userId && userBioId) {
        if (fullname === "") {
          res.status(400);
          return res.json({ message: "invalid fullname" });
        } else if (gender === "") {
          res.status(400);
          return res.json({ message: "invalid gender" });
        } else if (address === "") {
          res.status(400);
          return res.json({ message: "invalid address" });
        } else if (phoneNumber === "") {
          res.status(400);
          return res.json({ message: "invalid phone number" });
        }
        await userModel.updateUserBio(
          fullname,
          gender,
          address,
          phoneNumber,
          idUser
        );
        res.status(200);
        return res.json({ message: "User bio is updated" });
      } else {
        res.status(400);
        return res.json({ message: `User doesn't exist user_id ${idUser}` });
      }
    } catch (error) {
      console.log(error);
      res.status(400);
      return res.json({ message: `user doesn't exist` });
    }
  };

  // record user game status
  userGames = async (req, res) => {
    const { user_id, result } = req.body;
    const userId = await userModel.findUserId(user_id);
    if (!userId || !result) {
      res.status(400);
      return res.json({ message: `User not found or invalid result` });
    } else {
      res.status(200);
      await userModel.userGameHis(user_id, result);
      return res.json({ message: "Successfully added game history" });
    }
  };

  // Display user game history
  getUserHistory = async (req, res) => {
    const { idUser } = req.params;
    try {
      res.status(200);
      const userHistoryData = await userModel.getIdGameHistory(idUser);
      const userId = await userModel.findUserId(idUser);
      if (userId) {
        res.status(200);
        return res.json(userHistoryData);
      } else {
        res.status(404);
        return res.json({ message: "User in not found" });
      }
    } catch (error) {
      console.log(error);
      res.status(400);
      return res.json({ message: "User not found" });
    }
  };
}
module.exports = new UserController();
